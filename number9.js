// Check scaled up versions of known 8-15-17 Pythagorean triplets
// 8,15,17 , 16,30,34 , 27,36,45 , etc...
let a = 8;
let b = 15;
let c = 17;

for (let i = 1; i*(a+b+c) <= 1000; i++) {
  if (i*(a+b+c) === 1000) {
    a *= i;
    b *= i;
    c *= i;
    console.log(a*b*c);
  }
}
