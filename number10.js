// Determines if a number is prime
function isPrime(n) {
  for (let i = 3; i*i <= n; i += 2) {
    if (n % i === 0) {
      return false;
    }
  }
  return true;
}

// Accounts for 2 being prime
let sum = 2;

// If we find a prime number, increment sum
for (let i = 3; i < 2000000; i += 2) {
  if (isPrime(i)) {
    sum += i;
  }
}
console.log(sum);
