// Determines if a number is prime
function isPrime(n) {
  for (let i = 3; i*i <= n; i += 2) {
    if (n % i === 0) {
      return false;
    }
  }
  return true;
}

// Accounts for 2 being prime
let count = 1;

// The current prime number
let current = 3;

// if we find a number, increment count, update current prime
for (let i = 3; count < 10001; i += 2) {
  if (isPrime(i)) {
    count++;
    current = i;
  }
}
console.log(current);
