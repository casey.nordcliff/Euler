// Pretty easy, just nested-loop through all combinations, keeping track of
// palindromic products
let record = 0;
for (let i = 1; i < 1000; i++) {
  for (let j = 1; j < 1000; j++) {
    let product = (i *j);
    if (product > record) {
      product = product.toString();
      if (product === product.split("").reverse().join("")) {
        record = product;
      }
    }
  }
}

console.log(record);
