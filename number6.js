// Euler's theorum
// 1 + 2 + 3 + ...
function squareOfsumOfNumbers(n) {
  // (n * (n+1)) / 2 squared, simplified
  return (n*n*n*n + n*n)/4 + (n*n*n)/2;
}

// Square pyramidal theorum
// 1 + 4 + 9 + 16 + ...
function sumOfSquaresOfNumbers(n) {
  // (n * ((2 * n) + 1) * (n + 1)) / 6 simplified
  return (n*n*n)/3 + (n*n)/2 + n/6;
}

console.log(squareOfsumOfNumbers(100) - sumOfSquaresOfNumbers(100));
