let number = 600851475143;

// Keep track of factors as we find them
const factors = [];
for (let i = 2; i <= number; i++) {
  if (number % i === 0) {
    number = number / i;
    factors.push(i);
  }
}

// At this point we have all of the factors, we just need to filter out the
// non prime factors

function isPrime(n) {
  for (let i = 3; i*i <= n; i++) {
    if (n % i === 0) {
      return false;
    }
  }
  return true;
}

const primeFactors = [];

for (let i = 0; i < factors.length; i++) {
  if (isPrime(factors[i])) {
    primeFactors.push(factors[i]);
  }
}

// At this point, we have all the prime factors, and the one at the end will be
// the largest prime factor of 600851475143
console.log(primeFactors[primeFactors.length - 1]);
