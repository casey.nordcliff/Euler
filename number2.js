// Start off at 1 and 2
let nSubOne = 1;
let nSubTwo = 2;

// Accounts for starting with an even number
let sum = 2;

// Figure out the next number, add it to the sum if it's even
while (nSubTwo < 4000000) {
  let temp = nSubOne + nSubTwo;
  if (temp % 2 === 0) {
    sum += temp;
  }
  nSubOne = nSubTwo;
  nSubTwo = temp;
}

console.log(sum);
