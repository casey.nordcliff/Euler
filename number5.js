// List of factos 1-20. Being divisbile by 20 means being divisble by
// 1, 2, 4, 5, 10, and 20, so we eliminate composite factors, and just test the
// largest one. Do this for every number going back
const factors = [11,12,13,14,15,16,17,18,19,20];

// We know the answer is a multiple of 20, so we may as well start there, as
// well as increment by 20 each time
let current = 20;
while (true) {
  if (isDivByAll(current)) {
    break;
  }
  current += 20;
}

console.log(current);


function isDivByAll(n) {
  for (let i = 0; i < factors.length; i++) {
    if (!(n % factors[i] === 0)) {
      return false;
    }
  }
  return true;
}
